(defconstant +max-lessons-number+ 8 "Переменная показывает, сколько максимально может быть уроков в какой-либо день")

(defconstant +weeck-days-number+ 6 "Число учебных дней в неделе")

(defstruct lesson
  "Минимальная структура в иерархии расписания. Содржит в себе данные о "
  (subject nil) (teacher nil) (room nil))

(defstruct school-day
  (lessons (loop repeat +max-lessons-number+ collect (make-lesson))))

(defstruct school-weeck
  (days (loop repeat +weeck-days-number+ collect (make-school-day))))

(defstruct school-class
  number liter (weeck (make-school-weeck)))

(defstruct school-schedule
  classes) ;В ячейку classes нужно поместить список классов

(defun is-lesson-a-gap? (lesson)
  (not (null (lesson-subject lesson))))

(defun get-lesson-from-class (class day-num lesson-num)
  (elt (school-day-lessons (elt (school-weeck-days (school-class-weeck class))
				day-num)) 
       lesson-num))

(defun set-lesson-in-class (lesson class day-num lesson-num)
  (setf (elt (school-day-lessons (elt (school-weeck-days (school-class-weeck class))
				 day-num))
             lesson-num)
        lesson))


(defun eql-teachers (a-class a-day a-lesson b-class b-day b-lesson)
  (eql (lesson-teacher (get-lesson-from-class a-class a-day a-lesson))
       (lesson-teacher (get-lesson-from-class b-class b-day b-lesson))))


(defun collising-class? (class-a class-b day lesson)
  "Функция принемает два класса и указание на урок, после чего сравнивает учителей и комнаты, соответствующие этому уроку.
   Сравнение она производит отдельно для ячеек teacher и room. Если ячейка в одном или обоих классах равна nil то коллизии нет.
   Результат сравнения возвращается в виде списка. Первае ячейка показывает, есть ли коллизия учителей, вторая - классов."
  (list (and (not (null (lesson-teacher (get-lesson-from-class class-a day lesson))))
	     (not (null (lesson-teacher (get-lesson-from-class class-b day lesson))))
	     (eql (lesson-teacher (get-lesson-from-class class-a day lesson))
		  (lesson-teacher (get-lesson-from-class class-b day lesson))))

	(and (not (null (lesson-room (get-lesson-from-class class-a day lesson))))
	     (not (null (lesson-room (get-lesson-from-class class-b day lesson))))
	     (eql (lesson-room (get-lesson-from-class class-a day lesson))
		  (lesson-room (get-lesson-from-class class-b day lesson))))))

(defstruct collision
  first-class-number first-class-liter
  second-class-number second-class-liter
  day lesson
  teacher-collision
  room-collision)

(defun new-collision (first-class second-class day lesson teach room)
  (make-collision
   :first-class-number (school-class-number first-class) :first-class-liter (school-class-liter first-class)
   :second-class-number (school-class-number second-class) :second-class-liter (school-class-liter second-class)
   :day day :lesson lesson
   :teacher-collision teach
   :room-collision room))
   
(defun collisions-in-schedule (my-schedule)
  (let ((collisions nil))
    (loop for day from 0 to (1- +weeck-days-number+) do
          (loop for lesson from 0 to (1- +max-lessons-number+) do
            (do* ((class-list (school-schedule-classes my-schedule) (cdr class-list))
		  (first-class (car class-list) (car class-list)))
		 ((null class-list))
	      (dolist (second-class (cdr class-list))
		(let ((collis (collising-class? first-class second-class day lesson)))
		  (if (or (car collis) (cdr collis))
		      (push (new-collision first-class
					   second-class
					   day
					   lesson
					   (car collis)
					   (cdr collis))
			    collisions)))))))))
	  
